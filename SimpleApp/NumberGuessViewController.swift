//
//  NumberGuessViewController.swift
//  SimpleApp
//
//  Created by Boon Yee Yuen on 13/4/18.
//  Copyright © 2018 Transpire. All rights reserved.
//

import UIKit

class NumberGuessViewController: UIViewController {
    @IBOutlet weak var txtGuess: UITextField!
    @IBOutlet weak var lblMessage: UILabel!
    let magicNumber = 42
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func handleSubmitGuess(_ sender: Any) {
        if let guessedText = txtGuess.text,
            let guessedNumber = Int(guessedText) {
            if guessedNumber < magicNumber{
                lblMessage.text = "Higher!"
            }else if guessedNumber > magicNumber{
                lblMessage.text = "Lower!"
            }else{
                lblMessage.text = "That's the answer to the ultimate question."
            }
        }
    }
    
}

