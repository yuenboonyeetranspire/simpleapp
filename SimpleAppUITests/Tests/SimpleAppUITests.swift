//
//  SimpleAppUITests.swift
//  SimpleAppUITests
//
//  Created by Boon Yee Yuen on 13/4/18.
//  Copyright © 2018 Transpire. All rights reserved.
//

import XCTest

class SimpleAppUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    
    /// Verifies that low guesses, correct and high guesses produce the appropriate message.
    ///
    /// Done on the storyboard view controller with no accessibility identifiers.
    func testGuesses(){
        
        let app = XCUIApplication()
        app.staticTexts["Guess a number"].tap()
        
        let textField = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element
        textField.tap()
        
        let moreKey = app/*@START_MENU_TOKEN@*/.keys["more"]/*[[".keyboards",".keys[\"more, numbers\"]",".keys[\"more\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
        moreKey.tap()
        moreKey.tap()
        textField.typeText("2")
        app.buttons["Go!"].tap()
        app.staticTexts["Higher!"].tap()
    }
    
    /// Verifies that low guesses, correct and high guesses produce the appropriate message.
    ///
    /// Done on the storyboard view controller with no accessibility identifiers.
    func testGuessesUsingIdentifiers(){
        
        let app = XCUIApplication()
        app.tabBars.buttons["Has Accessibility"].tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["lblHeader"]/*[[".staticTexts[\"Guess a number\"]",".staticTexts[\"lblHeader\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let txtnumberTextField = app.textFields["txtNumber"]
        txtnumberTextField.tap()
        
        let moreKey = app/*@START_MENU_TOKEN@*/.keys["more"]/*[[".keyboards",".keys[\"more, numbers\"]",".keys[\"more\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
        moreKey.tap()
        moreKey.tap()
        txtnumberTextField.typeText("2")
        app/*@START_MENU_TOKEN@*/.buttons["btnGuess"]/*[[".buttons[\"Go!\"]",".buttons[\"btnGuess\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["lblResult"]/*[[".staticTexts[\"Higher!\"]",".staticTexts[\"lblResult\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        XCTAssertEqual(app.staticTexts["lblResult"].label, "Higher!")
        
    }

    /// Verifies that low guesses, correct and high guesses produce the appropriate message.
    ///
    /// Done on the storyboard view controller with no accessibility identifiers.
    func testGuessesUsingPageObject(){
        
        let app = XCUIApplication()
        app.tabBars.buttons["Has Accessibility"].tap()
        XCTAssertEqual(NumberGuessPage.headerLabel.label, "Guess a number")
        NumberGuessPage.guessANumber(guessNumber: 1)
        XCTAssertEqual(NumberGuessPage.resultMessageText(), "Higher!")
        NumberGuessPage.guessANumber(guessNumber: 2)
        XCTAssertEqual(NumberGuessPage.resultMessageText(), "Higher!")
        NumberGuessPage.guessANumber(guessNumber: 42)
        XCTAssertEqual(NumberGuessPage.resultMessageText(), "That's the answer to the ultimate question.")
        NumberGuessPage.guessANumber(guessNumber: 43)
        XCTAssertEqual(NumberGuessPage.resultMessageText(), "Lower!")
        NumberGuessPage.guessANumber(guessNumber: 44)
        //The following will fail and a screenshot will be included in the test report.
        XCTAssertEqual(NumberGuessPage.resultMessageText(), "Lower?")
    }
    
    func testTrivialExample() {
        
        let app = XCUIApplication()
        app.staticTexts["Guess a number"].tap()
        
        let textField = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element
        textField.tap()
        app.staticTexts["Make a guess!"].tap()
        
        let moreKey = app/*@START_MENU_TOKEN@*/.keys["more"]/*[[".keyboards",".keys[\"more, numbers\"]",".keys[\"more\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/
        moreKey.tap()
        moreKey.tap()
        textField.typeText("22")
        app.buttons["Go!"].tap()
        
    }
    
    func testTrivialExampleWithAccessibilityLabels() {
        
        let app = XCUIApplication()
        //The following will fail and a screenshot will be included in the test report.
        app/*@START_MENU_TOKEN@*/.staticTexts["lblHeader"]/*[[".staticTexts[\"Guess a number\"]",".staticTexts[\"lblHeader\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.textFields["txtNumber"].tap()
        app/*@START_MENU_TOKEN@*/.staticTexts["lblResult"]/*[[".staticTexts[\"This is read out to the user\"]",".staticTexts[\"lblResult\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.buttons["btnGuess"]/*[[".buttons[\"Go!\"]",".buttons[\"btnGuess\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
    }

}
