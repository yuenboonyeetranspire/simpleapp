//
//  NumberGuessPage.swift
//  SimpleAppUITests
//
//  Created by Boon Yee Yuen on 13/4/18.
//  Copyright © 2018 Transpire. All rights reserved.
//

import Foundation
import XCTest


/// Class to expose UI elements and operations on the NumberGuessViewController.
class NumberGuessPage{
    private static let app = XCUIApplication()
    
    /// Label at the top of the page.
    static let headerLabel = app.staticTexts["lblHeader"]
    
    /// Text field to enter the guess.
    static let numberEntryField = app.textFields["txtNumber"]
    
    /// Button to submit the guess.
    static let submitButton = app.buttons["btnGuess"]
    
    /// The message label for displaying guess results.
    static let resultMessage = app.staticTexts["lblResult"]
    
    class Alerts{
        class Warning{
            static let text = "Warning! Danger Will Robinson!"
        }
        
        class Confirmation{
            static let text = "Are you sure?"
        }
        
    }
    
    // MARK:- Methods
    
    /// Returns the text in the result message label.
    static func resultMessageText() -> String {
        return NumberGuessPage.resultMessage.label
    }
    
    
    /// Enters a number as a guess and taps on the submit button.
    ///
    /// - Parameter guessNumber: The number to guess.
    static func guessANumber(guessNumber:Int) {
        XCTContext.runActivity(named: "\(self).\(#function)-\(guessNumber)") { activity in
            NumberGuessPage.numberEntryField.tap()
            NumberGuessPage.numberEntryField.clearAndEnterText(text: "\(guessNumber)")
            NumberGuessPage.submitButton.tap()
        }
    }
}
